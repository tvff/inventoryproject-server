using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace InventoryProject.Models
{
	/// <summary>
	/// Represents an item. Duh.
	/// </summary>
	public class UniqueItem
	{
		public uint Id {get; set;}
		public uint OwnerId {get; set;}
		public long CreatedAt {get; set;}
		public long ModifiedAt {get; set;}
		public string Name {get; set;}
		public string Description {get; set;}
		public ICollection<Tag> Tags;
		internal static UniqueItem FromDBE(DBE.DBEItem item)
		{
			if (item == null)
				return null;
			return new UniqueItem{
				Id = item.Id,
				Name = item.Name,
				Description = item.Description,
				OwnerId = item.OwnerId,
				Tags = null,
				CreatedAt = item.CreatedAt,
				ModifiedAt = item.ModifiedAt
			};
		}
	}
	public class NewUniqueItem
	{
		[Required]
		public string Name {get; set;}
		public string Description {get; set;}
		public uint[] Tags;
	}
}
