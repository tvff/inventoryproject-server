// Basic models, used for API inputs & outputs.
namespace InventoryProject.Models
{
	public class EntityId
	{
		public uint Id {get; set;}
	}
}