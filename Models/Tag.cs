using System;
using System.ComponentModel.DataAnnotations;

namespace InventoryProject.Models
{
	///<summary>A tag that can be applied to a UniqueItem... and perhaps other objects.</summary>
	public class Tag
	{
		public uint Id {get; set;}
		public string Name {get; set;}
		public uint OwnerId {get; set;}
		internal static Tag FromDBE(DBE.DBETag tag)
		{
			return new Tag{
				Id = tag.Id,
				Name = tag.Name,
				OwnerId = tag.OwnerId
			};
		}
	}
	public class NewTag
	 {
		[Required]
		public string Name {get; set;}
	}
}