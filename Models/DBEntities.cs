// All the models that are in the database are here, the ones publicly visible in the API shall not be here.
using System.Collections.Generic;
namespace InventoryProject.Models.DBE
{
	public enum UserRole
	{
		User,
		Administrator
	}
	internal class DBEUser
	{
		public uint Id {get; set;}
		// SHA512 hashed
		public byte[] Password {get; set;}
		public string Username {get; set;}
		public string FriendlyName {get; set;}
		public UserRole Role {get; set;}
		public DBEUserSettings UserSettings {get; set;}
	}
	internal class DBEUserSettings
	{
		public uint Id {get; set;}
		public ICollection<DBEUserInventoryPin> Pins {get; set;}
	}
	internal class DBEUserInventoryPin
	{
		public uint Id {get; set;}
		public DBEInventory Inventory {get; set;}
		public uint InventoryId {get; set;}
	}
	internal class DBEUserSession
	{
		public uint Id {get; set;}
		public string Token {get; set;}
		public long Created {get; set;}
		public long Expires {get; set;}
		public string Name {get; set;}
		public DBEUser User {get; set;}
	}
	internal class DBETag
	{
		public uint Id {get; set;}
		public string Name {get; set;}
		public uint OwnerId {get; set;}
		public DBEUser Owner {get; set;}
	}
	internal class DBEItem
	{
		public uint Id {get; set;}
		public uint OwnerId {get; set;}
		public long CreatedAt {get; set;}
		public long ModifiedAt {get; set;}
		public DBEUser Owner {get; set;}
		public string Name {get; set;}
		public string Description {get; set;}
		public ICollection<DBETag> Tags;
	}
	internal class DBEInventory
	{
		public virtual ICollection<DBEOwnedItem> Items {get; set;}
		public uint Id {get; set;}
		public uint OwnerId {get; set;}
		public DBEUser Owner {get; set;}
		public bool WorldVisible {get; set;}
		public string Name {get; set;}
		public string Description {get; set;}
	}
	internal class DBEOwnedItem 
	{
		public uint Id {get; set;}
		public uint ItemId {get; set;}
		public DBEItem Item {get; set;}
		public uint InventoryId {get; set;}
		public DBEInventory Inventory {get; set;}
		public uint Amount {get; set;}
	}
	internal class DBEItemHistoryEvent
	{
		public uint Id {get; set;}
		public uint ItemId {get; set;}
		public long Time {get; set;}
		public string Action {get; set;}

		public DBEOwnedItem Item {get; set;}
		public uint UserId {get; set;}
		public DBEUser User {get; set;}
		public uint InventoryId {get; set;}
		public DBEInventory Inventory {get; set;}
		public uint OtherUserId {get; set;}
		public DBEUser OtherUser {get; set;}
		public uint OtherInventoryId {get; set;}
		public DBEInventory OtherInventory {get; set;}
	}

	internal class DBETradeOffer
	{
		public uint Id {get; set;}
		public DBEUser Sender {get; set;}
		public uint SenderId {get; set;}
		public DBEUser Receiver {get; set;}
		public uint ReceiverId {get; set;}
		public DBEInventory DestinationInventory {get; set;}
		public uint? DestinationInventoryId {get; set;}
		public long Time {get; set;}
		public virtual ICollection<DBETradeItem> Items {get; set;}
		public ETradeType Type {get; set;}
		public string Message {get; set;}
	}
	internal class DBETradeItem
	{
		public uint Id {get; set;}
		public uint TradeOfferId {get; set;}
		public DBETradeOffer TradeOffer {get; set;}
		public uint ItemId {get; set;}
		public DBEOwnedItem Item {get; set;}
		public bool Offered {get; set;}
		internal DBETradeItem(DBEOwnedItem item, DBETradeOffer offer)
		{
			TradeOffer = offer;
			Item = item;
		}
		internal DBETradeItem() {}
	}
}
