using System.Collections.Generic;
namespace InventoryProject.Models
{
	public sealed class ApiErrorCode
	{
		public string Message {get;}
		public string Code {get;}
		public ApiErrorCode(string code, string message)
		{
			Code = code;
			Message = message;
		}
	}
	public static class ApiErrorCodes
	{
		// Still not happy with this, but at least I get completion now!
		public static ApiErrorCode BadLogin = new ApiErrorCode("bad_login", "No such user or wrong password.");
		public static ApiErrorCode BadLoginInfo = new ApiErrorCode("bad_logininfo", "Need username and password.");
		public static ApiErrorCode ItemNotFound = new ApiErrorCode("item_not_found", "Could not find that item.");

		public static ApiErrorCode BadInputData = new ApiErrorCode("bad_input_data", "Bad input data. You need to fill in all the fields!");
		public static ApiErrorCode InventoryNotFound = new ApiErrorCode("inventory_not_found", "Could not find that inventory.");
		public static ApiErrorCode ItemOrInventoryNotFound = new ApiErrorCode("item_or_inventory_not_found", "Could not find that item or inventory.");
		public static ApiErrorCode TagNotFound = new ApiErrorCode("tag_not_found", "Could not find a tag with that id.");
		public static ApiErrorCode CantLogOutAsGuest = new ApiErrorCode("cant_log_out_as_guest", "You need to log in to log out.");
		public static ApiErrorCode NeedAuth = new ApiErrorCode("need_auth", "You need to be logged in to do that.");
		public static ApiErrorCode UsernameTaken = new ApiErrorCode("username_taken", "That username has already been taken!");
		public static ApiErrorCode UserNotFound = new ApiErrorCode("user_not_found", "Could not find that user.");
		public static ApiErrorCode NotAllowed = new ApiErrorCode("not_allowed", "You're not allowed to do that.");
		public static ApiErrorCode WrongPassword = new ApiErrorCode("wrong_password", "Wrong password.");
		public static ApiErrorCode AlreadyPinned = new ApiErrorCode("already_pinned", "Can't pin something that's already pinned.");
		public static ApiErrorCode NotPinned = new ApiErrorCode("not_pinned", "Can't unpin something that's not pinned.");
		public static ApiErrorCode Generic = new ApiErrorCode("general_error", "Something happened.");
		public static ApiErrorCode TradeOfferNotFound = new ApiErrorCode("trade_offer_not_found", "Could not find that trade offer.");
	}
	///<summary>An API error, when the user screws up.</summary>
	public class ApiError
	{
		public string Message {get;}
		public string Error {get;}
		public ApiError(ApiErrorCode errorcode)
		{
			Error = errorcode.Code;
			Message = errorcode.Message;
		}
	}
}
