using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Collections.Generic;
namespace InventoryProject.Models
{
	public enum ETradeType
	{
		Trade,
		Loan
	}
	///<summary>For creating a new trade offer.</summary>
	public class NewTradeOffer
	{
		public uint[] DesiredItems {get; set;}
		public uint[] OfferedItems {get; set;}
		public ETradeType Type {get; set;}
		[Required]
		public string Message {get; set;}
		public uint Receiver {get; set;}
		///<summary>If any items are desired then this is required to specify where to put the items if the offer is accepted.</summary>
		public uint? Destination {get; set;}
	}
	public class TradeOffer
	{
		public uint Id {get; set;}
		public OwnedItem[] DesiredItems {get; set;}
		public OwnedItem[] OfferedItems {get; set;}
		public ETradeType Type {get; set;}
		[Required]
		public string Message {get; set;}
		public long Time {get; set;}
		public uint Sender {get; set;}
		public uint Receiver {get; set;}

		internal static TradeOffer FromDBE(DBE.DBETradeOffer offer)
		{
			// Notice how the items have to be added manually.
			return new TradeOffer{
				Id = offer.Id,
				Type = offer.Type,
				Message = offer.Message,
				Time = offer.Time,
				Sender = offer.SenderId,
				Receiver = offer.ReceiverId
			};
		}
	}
	public class TradeOfferResponse
	{
		// Yeah.. not using an enum for this. I'm consistent like that.
		///<summary>"accept" to accept the offer, "decline" to say no thanks.</summary>
		[Required]
		public string Response {get; set;}
		///<summary>If accepting the offer, send the items to this inventory.</summary>
		public uint? Destination {get; set;}
	}
}