using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using InventoryProject.Models.DBE;
namespace InventoryProject.Models
{
	// This mega DbContext contains all the things needed.
	// This isn't really web scale.
	public class InvDbContext : DbContext
	{
		internal DbSet<DBEItem> Items {get;private set;}
		internal DbSet<DBEUser> Users{get;private set;}
		internal DbSet<DBEUserSettings> UserSettings {get; private set;}
		internal DbSet<DBEUserInventoryPin> Pins {get; private set;}
		internal DbSet<DBEInventory> Inventories{get;private set;} 
		internal DbSet<DBEOwnedItem> OwnedItems{get; private set;}
		internal DbSet<DBEUserSession> Sessions{get; private set;}
		internal DbSet<DBETag> Tags {get; private set;}
		internal DbSet<DBETradeOffer> TradeOffers {get; private set;}
		internal DbSet<DBETradeItem> TradeItems {get; private set;}
		public InvDbContext(DbContextOptions<InvDbContext> opt) : base(opt)
		{

		}
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<DBETradeOffer>().Property(o => o.DestinationInventoryId).IsRequired(false);
		}
		internal static void Configure(DbContextOptionsBuilder opt)
		{
			var config = AppConfiguration.Instance;
			switch (config.DatabaseBackend) {
				case EFDatabaseBackend.InMemory:
					opt.UseInMemoryDatabase("Inventorium");
					break;
				case EFDatabaseBackend.MySQL:
					opt.UseMySql(config.DBConnectionString);
					break;
				case EFDatabaseBackend.SQLite:
					opt.UseSqlite(config.DBConnectionString);
					break;
			}

		}
	}
	public class InvDbContextFactory : IDesignTimeDbContextFactory<InvDbContext>
	{
		// This is some disgusting hacky hack to avoid hardcoding the connection string.
		// Because the dotnet ef commands are weird like that.
		// And yes I repeat myself here.
		public InvDbContext CreateDbContext(string[] args)
		{
			var optsBuilder = new DbContextOptionsBuilder<InvDbContext>();
			AppConfiguration.Instance.LoadConfig("config.json").GetAwaiter().GetResult();
			InvDbContext.Configure(optsBuilder);
			return new InvDbContext(optsBuilder.Options);
		}
	}
}
