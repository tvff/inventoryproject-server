using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System;

namespace InventoryProject.Models
{

	public class User
	{
		public uint Id {get; set;}
		public string Username {get; set;}
		public string FriendlyName {get; set;}
		public DBE.UserRole Role {get; set;}
		internal static User FromDBE(DBE.DBEUser user)
		{
			if (user == null)
				return null;
			return new User {
				Id = user.Id,
				Username = user.Username,
				FriendlyName = user.FriendlyName,
				Role = user.Role
			};
		}
	}
	public class NewUserInfo
	{
		[Required]
		public string Username {get; set;}
		[Required]
		public string FriendlyName {get; set;}
		[Required]
		public string Password {get; set;}
	}
	public class ChangePasswordInfo
	{
		[Required]
		public string OldPassword {get; set;}
		[Required]
		public string Password {get; set;}
	}
	public class LogInInfo
	{
		[Required]
		public string Username {get; set;}
		[Required]
		public string Password {get; set;}
	}
	public class LogOutInfo
	{
		public bool AllSessions {get; set;}
	}
	public class SessionAndUser
	{
		public UserSession Session  {get; set;}
		public User User {get; set;}
	}
	public class UserSessionLite
	{
		public uint Id {get; set;}
		public long Created {get; set;}
		public long Expires {get; set;}
		public string Name {get; set;}
		internal static UserSessionLite FromDBE(DBE.DBEUserSession session)
		{
			return new UserSessionLite{
				Id = session.Id,
				Created = session.Created,
				Expires = session.Expires,
				Name = session.Name
			};
		}
	}
	public class UserSession
	{
		public uint Id {get; set;}
		public long Created {get; set;}
		public long Expires {get; set;}
		public string Name {get; set;}
		public string Token {get; set;}
		internal static UserSession FromDBE(DBE.DBEUserSession session)
		{
			return new UserSession{
				Id = session.Id,
				Created = session.Created,
				Expires = session.Expires,
				Name = session.Name,
				Token = session.Token
			};
		}
	}
	public class UserSessionData
	{
		public IEnumerable<UserSessionLite> Sessions {get; set;}
		public uint CurrentSession {get; set;}
		public User User {get; set;}
	}
}
