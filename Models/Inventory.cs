using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
namespace InventoryProject.Models
{
	/// <summary>Represents an inventory holding OwnedItems.</summary>
	public class Inventory
	{
		public uint Id {get; set;}
		[Required]
		public string Name {get; set;}
		public string Description {get; set;}
		public uint OwnerId {get; set;}
		public virtual ICollection<OwnedItem> Items {get; set;}
		///<summary>Whether this inventory is public and can be seen by all.</summary>
		public bool WorldVisible {get; set;}
		internal static Inventory FromDBE(DBE.DBEInventory inventory)
		{
			ICollection<OwnedItem> items = null;
			if (inventory.Items != null)
				items = inventory.Items.Select(i => OwnedItem.FromDBE(i)).ToList();
			return new Inventory{
				Id = inventory.Id,
				Name = inventory.Name,
				OwnerId = inventory.OwnerId,
				Items = items,
				WorldVisible = inventory.WorldVisible,
				Description = inventory.Description
			};
		}
	}
	/// <summary>Data needed to create a new inventory, or modify one.</summary>
	public class NewInventory
	{
		[Required]
		public string Name {get; set;}
		public string Description {get; set;}
		public bool? WorldVisible {get; set;}
	}
	/// <summary>An inventory without its items.</summary>
	public class InventoryBase
	{
		public uint Id {get; set;}
		public string Name {get; set;}
		public string Description {get; set;}
		public bool WorldVisible {get; set;}
		public uint OwnerId {get; set;}
	}
	/// <summary>Represents a unique instance of an item owned by an Inventory.</summary>
	public class OwnedItem 
	{
		public uint Id {get; set;}
		public uint ItemId {get; set;}
		public uint InventoryId {get; set;}
		public uint Amount {get; set;}
		public static OwnedItem FromUnique(UniqueItem item)
		{
			return new OwnedItem
			{
				ItemId = item.Id,
			};
		}
		internal static OwnedItem FromDBE(DBE.DBEOwnedItem item)
		{
			return new OwnedItem {
				Id = item.Id,
				ItemId = item.ItemId,
				InventoryId = item.InventoryId,
				Amount = item.Amount
			};
		}
	}
}
