using System;
using System.IO;
using System.Threading.Tasks;

namespace InventoryProject
{
	public enum EFDatabaseBackend
	{
		InMemory,
		MySQL,
		SQLite
	}
	internal static class JsonExtensions
	{
		internal static string GetStringProp(this System.Text.Json.JsonElement element, string propname, string defaultvalue)
		{
			if (element.TryGetProperty(propname,out var prop)){
				return prop.GetString();
			}
			return defaultvalue;
		}
	}
	public class AppConfiguration
	{
		public static  AppConfiguration Instance {get;} = new AppConfiguration();
		public string DBConnectionString {get; set;}
		public EFDatabaseBackend DatabaseBackend {get; set;} = EFDatabaseBackend.InMemory;
		private void LogError(string msg)
		{
			Console.Error.WriteLine("[config] " + msg);
		}
		private void SetBackend(string backend)
		{
			switch (backend) {
				case "inmemory": DatabaseBackend = EFDatabaseBackend.InMemory; return;
				case "mysql": DatabaseBackend = EFDatabaseBackend.MySQL; return;
				case "sqlite": DatabaseBackend = EFDatabaseBackend.SQLite; return;
				default: LogError($"Unknown EF backend: {backend}"); return;
			}
		}
		public async Task LoadConfig(string filename)
		{
			try {
				var file = File.OpenRead(filename);
				using (var json = await System.Text.Json.JsonDocument.ParseAsync(file)) {
					SetBackend(json.RootElement.GetStringProp("efbackend","inmemory"));
					DBConnectionString = json.RootElement.GetStringProp("dbconnectionstring",string.Empty);
					if (DBConnectionString == string.Empty)
						LogError("Empty DB connection string!");
				}
			}
			catch (Exception e) {
				LogError($"Something went wrong loading config file {filename}: {e.Message}");
			}
		}
	}
}
