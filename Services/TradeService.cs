using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryProject.Models;
using InventoryProject.Models.DBE;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace InventoryProject.Services
{
	///<summary>
	/// Everything related to trading in this service.
	///</summary>
	public class TradeService 
	{
		private readonly InvDbContext DbContext;
		public TradeService(InvDbContext context)
		{
			DbContext = context;
		}
		public async Task<TradeOffer> Get(uint id, uint userId)
		{
			var doffer = await DbContext.TradeOffers.AsNoTracking().Include(o => o.Items).ThenInclude(o => o.Item)
			.SingleOrDefaultAsync(o => (o.SenderId == userId || o.ReceiverId == userId) && o.Id == id);
			if (doffer == null)
				return null;
			return DBETradeOfferToTradeOffer(doffer);
		}
		private TradeOffer DBETradeOfferToTradeOffer(DBETradeOffer dbeoffer)
		{
			var offer = TradeOffer.FromDBE(dbeoffer);
			var groups = dbeoffer.Items.GroupBy(i => i.Offered);
			var desired = new List<OwnedItem>();
			var offered = new List<OwnedItem>();
			// This is horrible.
			foreach (var g in groups){
				foreach (var item in g){
					if (g.Key)
						offered.Add(OwnedItem.FromDBE(item.Item));
					else
						desired.Add(OwnedItem.FromDBE(item.Item));
				}
			}
			offer.DesiredItems = desired.ToArray();
			offer.OfferedItems = offered.ToArray();
			return offer;
		}
		public async Task<IEnumerable<TradeOffer>> GetAll(uint userId)
		{
			var list = await DbContext.TradeOffers.AsNoTracking().Where(o => o.SenderId == userId || o.ReceiverId == userId).ToListAsync();
			return list.Select(o => TradeOffer.FromDBE(o));
		}
		private async Task<ICollection<DBEInventory>> GetInventories(ICollection<DBEOwnedItem> items)
		{
			var groups = items.GroupBy(e => e.InventoryId);
			var invs = new List<uint>();
			foreach (var g in groups){
				invs.Add(g.Key);
			}
			return await DbContext.Inventories.AsNoTracking().Where(i => invs.Any(e => e == i.Id)).ToListAsync();
		}
		public async Task<TradeOffer> New(NewTradeOffer offer, uint senderId)
		{
			List<DBEOwnedItem> desired = null,offered = null;
			if (offer.DesiredItems.Length > 0)
				desired = await DbContext.OwnedItems.Where(o => offer.DesiredItems.Any(e => e == o.Id)).ToListAsync();
			if (offer.OfferedItems.Length > 0)
				offered = await DbContext.OwnedItems.Where(o => offer.OfferedItems.Any(e => e == o.Id)).ToListAsync();
			// This ain't great.
			if (offered != null){
				var invs = await GetInventories(offered);
				if (!invs.All(i => i.OwnerId == senderId))
					return null;
			}
			if (desired != null){
				var invs = await GetInventories(desired);
				if (!invs.All(i => i.OwnerId == offer.Receiver && i.WorldVisible))
					return null;
				if (!offer.Destination.HasValue)
					return null;
			}
			var newoffer = new DBETradeOffer{
				Message = offer.Message,
				SenderId = senderId,
				ReceiverId = offer.Receiver,
				Time = DateTimeOffset.UtcNow.ToUnixTimeSeconds(),
				Type = offer.Type,
				DestinationInventoryId = offer.Destination.Value
			};
			if (desired != null)
				newoffer.Items = desired.Select(i => new DBETradeItem(i,newoffer)).ToList();
			if (offered != null){
				foreach (var item in offered.Select(i => new DBETradeItem(i,newoffer){Offered=true})){
					newoffer.Items.Add(item);
				}
			}
			if (newoffer.Items.Count == 0)
				return null;
			await DbContext.TradeOffers.AddAsync(newoffer);
			await DbContext.SaveChangesAsync();
			return DBETradeOfferToTradeOffer(newoffer);
		}
		public async Task<TradeOffer> Delete(uint offerId, uint userId)
		{
			var offer = await DbContext.TradeOffers.FindAsync(offerId);
			if (offer != null && offer.SenderId == userId){
				DbContext.TradeOffers.Remove(offer);
				await DbContext.SaveChangesAsync();
				return TradeOffer.FromDBE(offer);
			}
			return null;
		}
		public async Task<TradeOffer> AcceptOffer(uint offerId, uint userId, uint? destinationId)
		{
			var offer = await DbContext.TradeOffers.Include(i => i.Items).
			ThenInclude(t => t.Item).Include(i => i.DestinationInventory)
				.SingleOrDefaultAsync(i => i.Id == offerId);
			if (offer != null && offer.ReceiverId == userId){
				var destInv = await DbContext.Inventories.FindAsync(destinationId);
				if (offer.Items.Any(i => i.Offered)&&(destInv == null || destInv.OwnerId != userId)){
					System.Console.WriteLine("!!!! FAILING BECAUSE REASONS!");
					return null;
				}
				foreach (var item in offer.Items){
					if (item.Offered)
						item.Item.InventoryId = destInv.Id;
					else
						item.Item.InventoryId = offer.DestinationInventory.Id;
				}
				DbContext.TradeOffers.Remove(offer);
				await DbContext.SaveChangesAsync();
				return TradeOffer.FromDBE(offer);
			}
			return null;
		}
		public async Task<TradeOffer> RejectOffer(uint offerId, uint userId)
		{
			// This should actually be saved as a rejected offer rather than just being removed.
			var offer = await DbContext.TradeOffers.FindAsync(offerId);
			if (offer != null && offer.ReceiverId == userId){
				DbContext.TradeOffers.Remove(offer);
				await DbContext.SaveChangesAsync();
				return TradeOffer.FromDBE(offer);
			}
			return null;
		}
	}
}
