using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryProject.Models;
using InventoryProject.Models.DBE;
using Microsoft.EntityFrameworkCore;

namespace InventoryProject.Services
{
	///<summary>
	/// This is the main service, it does (hopefully) all data retrieval and manipulation.
	/// It may or may not get split up into separate services each with their own responsibilities.
	///</summary>
	public class InventoryService
	{
		private readonly InvDbContext DbContext;
		public InventoryService(InvDbContext context)
		{
			DbContext = context;
		}
		public async Task<UniqueItem> GetItem(uint id)
		{
			var item = await DbContext.Items.FindAsync(id);
			return UniqueItem.FromDBE(item);
		}
		public async Task<OwnedItem> GetOwnedItem(uint id, uint userId)
		{
			var item = await DbContext.OwnedItems.AsNoTracking().Include(i => i.Inventory).SingleOrDefaultAsync(i => i.Id == id);
			if (item != null && (item.Inventory.OwnerId == userId || item.Inventory.WorldVisible)){
				return OwnedItem.FromDBE(item);
			}
			return null;
		}
		public async Task<UniqueItem> GetItemWithTags(uint id)
		{
			try {
				return UniqueItem.FromDBE(await DbContext.Items.Include(i => i.Tags).FirstAsync(i => i.Id == id));
			}
			catch (Exception) {
				return null;
			}
		}
		public async Task<IEnumerable<UniqueItem>> GetAllItems()
		{
			return (await DbContext.Items.ToListAsync()).Select(i => UniqueItem.FromDBE(i)).ToList();
		}
		public async Task<UniqueItem> AddItem(NewUniqueItem item,uint ownerid)
		{
			var curTime = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
			var newItem = new DBEItem {
				Name = item.Name,
				Tags = new List<DBETag>(),
				OwnerId = ownerid,
				CreatedAt = curTime,
				ModifiedAt = curTime
			};
			if (item.Tags != null && item.Tags.Count() > 0) {
				var tags = await DbContext.Tags.Where(i => item.Tags.Any(b => b == i.Id)).ToListAsync();
				foreach (var tag in tags) {
					newItem.Tags.Add(tag);
				}
			}
			DbContext.Items.Add(newItem);
			await DbContext.SaveChangesAsync();
			return UniqueItem.FromDBE(newItem);
		}
		public async Task<UniqueItem> EditItem(uint id, NewUniqueItem item)
		{
			var oldItem = await DbContext.Items.FindAsync(id);
			if (oldItem == null)
				throw new Exception("Eeek! Item not found!");
			oldItem.Name = item.Name;
			oldItem.Description = item.Description;
			oldItem.ModifiedAt = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
			await DbContext.SaveChangesAsync();
			return UniqueItem.FromDBE(oldItem);
		}
		public async Task<UniqueItem> DeleteItem(uint id)
		{
			var item = await DbContext.Items.FindAsync(id);
			DbContext.Items.Remove(item);
			// Clean up all the owned items..
			DbContext.OwnedItems.RemoveRange(DbContext.OwnedItems.Where(i => i.ItemId == id));
			await DbContext.SaveChangesAsync();
			return UniqueItem.FromDBE(item);
		}
		public async Task<Inventory> GetInventory(uint id)
		{
			return Inventory.FromDBE(await DbContext.Inventories.FindAsync(id));
		}
		public async Task<Inventory> GetInventoryWithContent(uint id)
		{
			var inv = await DbContext.Inventories.AsNoTracking().Include(i => i.Items).SingleOrDefaultAsync(i => i.Id == id);
			if (inv == null)
				return null;
			return Inventory.FromDBE(inv);
		}
		public async Task<Inventory> CreateInventory(NewInventory inventory, uint ownerId)
		{
			var newInventory = new DBEInventory{
				Name = inventory.Name,
				Items = new List<DBEOwnedItem>(),
				OwnerId = ownerId,
				Description = inventory.Description,
				// Default to having privacy. I need my privacy... yeeaahh yeeah..
				WorldVisible = inventory.WorldVisible.GetValueOrDefault(false)
			};
			DbContext.Inventories.Add(newInventory);
			await DbContext.SaveChangesAsync();
			return Inventory.FromDBE(newInventory);
		}
		public async Task<Inventory> EditInventory(uint id, NewInventory data)
		{
			var inv = await DbContext.Inventories.FindAsync(id);
			inv.Name = data.Name;
			inv.Description = data.Description;
			inv.WorldVisible = data.WorldVisible.GetValueOrDefault(false);
			await DbContext.SaveChangesAsync();
			return Inventory.FromDBE(inv);
		}
		public async Task<Inventory> DeleteInventory(uint invId)
		{
			var inventory = await DbContext.Inventories.FindAsync(invId);
			if (inventory == null)
				return null;
			DbContext.OwnedItems.RemoveRange(DbContext.OwnedItems.Where(i => i.InventoryId == invId));
			DbContext.Inventories.Remove(inventory);
			await DbContext.SaveChangesAsync();
			return Inventory.FromDBE(inventory);
		}
		public async Task<OwnedItem> AddItemToInventory(uint inventoryId, uint itemId)
		{
			var item = await DbContext.Items.FindAsync(itemId);
			var inventory = await DbContext.Inventories.Include(r => r.Items).SingleOrDefaultAsync(r => r.Id == inventoryId);
			var newDbe = new DBEOwnedItem {
				InventoryId = inventory.Id,
				Item = item,
				Amount = 1
			};
			await DbContext.OwnedItems.AddAsync(newDbe);
			inventory.Items.Add(newDbe);
			await DbContext.SaveChangesAsync();
			return OwnedItem.FromDBE(newDbe);
		}
		public async Task<OwnedItem> RemoveItemFromInventory(uint inventoryId, uint itemId)
		{
			var inventory = await DbContext.Inventories.Include(r => r.Items).ThenInclude(it => it.Item).SingleOrDefaultAsync(r => r.Id == inventoryId);
			var owneditem = inventory.Items.SingleOrDefault(i => i.Id == itemId);
			inventory.Items.Remove(owneditem);
			await DbContext.SaveChangesAsync();
			return OwnedItem.FromDBE(owneditem);
		}
		public async Task<IEnumerable<InventoryBase>> GetInventories()
		{
			var listed = DbContext.Inventories.Where(i => i.WorldVisible);
			return await SelectInventories(listed).ToListAsync();
		}
		public async Task<IEnumerable<InventoryBase>> GetInventories(uint ownerId)
		{
			var listed = DbContext.Inventories.Where(i => i.WorldVisible || i.OwnerId == ownerId);
			return await SelectInventories(listed).ToListAsync();
		}
		private IQueryable<InventoryBase> SelectInventories(IQueryable<DBEInventory> invs)
		{
			return invs.Select(i => new InventoryBase{
				Name = i.Name,Id = i.Id,OwnerId = i.OwnerId,Description = i.Description, WorldVisible = i.WorldVisible
				});
		}
		public async Task<Tag> GetTag(uint id)
		{
			return Tag.FromDBE(await DbContext.Tags.FindAsync(id));
		}
		public async Task<IEnumerable<Tag>> GetTags(uint[] ids)
		{
			return (await GetDBETags(ids)).Select(i => Tag.FromDBE(i));
		}
		private async Task<IEnumerable<DBETag>> GetDBETags (uint[] ids)
		{
			return await DbContext.Tags.AsNoTracking().Where(i => ids.Any(b => b == i.Id)).ToListAsync();
		}
		public async Task<IEnumerable<Tag>> GetTags()
		{
			return (await DbContext.Tags.ToListAsync()).Select(t => Tag.FromDBE(t));
		}
		public async Task<UniqueItem> AddTagsToItem(uint id, uint[] tags)
		{
			var item = await DbContext.Items.Include(i => i.Tags).SingleOrDefaultAsync(i => i.Id == id);
			if (item == null)
				return null;
			var realTags = await GetDBETags(tags);
			foreach (var tag in realTags) {
				if (!item.Tags.Contains(tag))
					item.Tags.Add(tag);
			}
			await DbContext.SaveChangesAsync();
			return UniqueItem.FromDBE(item);
		}
		public async Task<UniqueItem> RemoveTagsFromItem(uint id, uint[] tags)
		{
			var item = await DbContext.Items.Include(i => i.Tags).SingleOrDefaultAsync(i => i.Id == id);
			if (item == null)
				return null;
			foreach (var tag in tags) {
				var realTag = item.Tags.FirstOrDefault(i => i.Id == tag);
				if (realTag != null)
					item.Tags.Remove(realTag);
			}
			await DbContext.SaveChangesAsync();
			return UniqueItem.FromDBE(item);
		}
		public async Task<Tag> AddTag(NewTag data)
		{
			var newTag = new DBETag{
				Name = data.Name
			};
			await DbContext.Tags.AddAsync(newTag);
			return Tag.FromDBE(newTag);
		}
		public async Task<Tag> DeleteTag(uint id)
		{
			var tag = await DbContext.Tags.FindAsync(id);
			if (tag == null)
				throw new KeyNotFoundException();
			DbContext.Tags.Remove(tag);
			await DbContext.SaveChangesAsync();
			return Tag.FromDBE(tag);
		}
	}
}
