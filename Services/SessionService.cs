using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryProject.Models;
using InventoryProject.Models.DBE;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace InventoryProject.Services
{
	///<summary>
	/// Handles sessions.
	///</summary>
	public class SessionService 
	{
		private readonly InvDbContext DbContext;

		public SessionService(InvDbContext context)
		{
			DbContext = context;
		}
		public async Task<IEnumerable<UserSession>> GetAll()
		{
			return (await DbContext.Sessions.ToListAsync()).Select(s => UserSession.FromDBE(s));
		}
		internal async Task<DBEUserSession> GetSession(HttpRequest request)
		{
			var token = request.Headers["X-Session"];
			try {
				if (token != String.Empty) {
					var sess = await GetSession(token);
					return sess;
				}
			}
			catch (InvalidOperationException) { }
			return null;
		}
		internal async Task<DBEUserSession> GetSession(string token)
		{
			if (token == String.Empty)
				return null;
			try {
				var sess = await DbContext.Sessions.Include(i => i.User).FirstOrDefaultAsync(s => s.Token == token);
				return sess;
			}
			catch (Exception e) {
				//oh shit!
				System.Console.WriteLine($"SHIT! Something went wrong getting the session.. {e}");
			}
			return null;
		}
		public async Task<UserSession> EndSession(uint sessionId)
		{
			var session = await DbContext.Sessions.FindAsync(sessionId);
			DbContext.Sessions.Remove(session);
			await DbContext.SaveChangesAsync();
			return UserSession.FromDBE(session);
		}
		public async Task EndAllSessions(uint userId)
		{
			var res = await DbContext.Sessions.Where(s => s.User.Id == userId).ToListAsync();
			DbContext.Sessions.RemoveRange(res);
			await DbContext.SaveChangesAsync();
		}
		public async Task<IEnumerable<UserSessionLite>> GetSessions(uint userId)
		{
			var sessions = await DbContext.Sessions.Where(s => s.User.Id == userId).ToListAsync();
			return sessions.Select(s => UserSessionLite.FromDBE(s));
		}
		public async Task<SessionAndUser> GenerateSessionForUser(uint userId,string name)
		{
			var expiryTime = DateTimeOffset.UtcNow.AddDays(2).ToUnixTimeSeconds();
			var rand = new Random();
			var randBytes = new byte[100];
			rand.NextBytes(randBytes);
			var token = Convert.ToBase64String(randBytes);
			var newSession = new DBEUserSession {
				User = await DbContext.Users.FindAsync(userId),
				Token = token,
				Created = DateTimeOffset.UtcNow.ToUnixTimeSeconds(),
				Expires = expiryTime,
				Name = name
			};
			DbContext.Sessions.Add(newSession);
			await DbContext.SaveChangesAsync();
			return new SessionAndUser{
				Session = UserSession.FromDBE(newSession),
				User = User.FromDBE(newSession.User)
			};
		}
	}
}
