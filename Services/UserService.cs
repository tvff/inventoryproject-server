using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using System.Security.Cryptography;
using InventoryProject.Models;
using InventoryProject.Models.DBE;
using Microsoft.EntityFrameworkCore;
namespace InventoryProject.Services
{
	public enum EPinResult{
		InventoryNotFound,
		AlreadyPinned,
		NotPinned,
		Ok
	}
	///<summary>
	/// This service does all the user related things.
	/// e.g. authentication, registering new users...
	///</summary>
	public class UserService
	{
		private readonly InvDbContext DbContext;
		private readonly SHA512 ShaHasher;

		public UserService(InvDbContext context)
		{
			ShaHasher = SHA512.Create();
			DbContext = context;
		}
		public void Init()
		{
			var hash = ComputeHash("secret");
			if (DbContext.Users.Count() == 0) {
				var user = new DBEUser{
					Username = "root",
					Password = hash,
					FriendlyName = "Root!",
					Role = UserRole.Administrator,
					UserSettings = new DBEUserSettings{
						Pins = new List<DBEUserInventoryPin>()
					}
				};
				DbContext.Add(user);
				DbContext.SaveChanges();
			}
		}
		private byte[] ComputeHash(string input)
		{
			return ShaHasher.ComputeHash(Encoding.UTF8.GetBytes(input));
		}
		public async Task<IEnumerable<User>> GetAll()
		{
			return await DbContext.Users.Select(u => User.FromDBE(u)).ToListAsync();
		}
		public async Task<User> GetUser(string username)
		{
			var user = await DbContext.Users.SingleOrDefaultAsync(u => u.Username == username);
			return User.FromDBE(user);
		}
		public async Task<User> GetUser(uint id)
		{
			var user = await DbContext.Users.FindAsync(id);
			if (user != null)
				return User.FromDBE(user);
			return null;
		}
		public async Task<User> Authenticate(LogInInfo info)
		{
			try {
				var hashedPassword = ShaHasher.ComputeHash(Encoding.UTF8.GetBytes(info.Password));
				var user = await DbContext.Users.SingleOrDefaultAsync(u => u.Username == info.Username);
				if (user != null && user.Password.SequenceEqual(hashedPassword))
					return User.FromDBE(user);
				return null;
			}
			catch (Exception) {
				return null;
			}
		}
		public async Task<User> CreateUser(NewUserInfo info)
		{
			var hashedPassword = ComputeHash(info.Password);
			var newUser = DbContext.Users.Add(new DBEUser{
				Username = info.Username,
				Password = hashedPassword,
				FriendlyName = info.FriendlyName,
				Role = UserRole.User,
				UserSettings = new DBEUserSettings{
					Pins = new List<DBEUserInventoryPin>()
				}
			});
			await DbContext.SaveChangesAsync();
			return User.FromDBE(newUser.Entity);
		}
		public async Task<bool> ChangePassword(uint userId, ChangePasswordInfo info)
		{
			var user = await DbContext.Users.FindAsync(userId);
			var oldhashed = ShaHasher.ComputeHash(Encoding.UTF8.GetBytes(info.OldPassword));
			if (!user.Password.SequenceEqual(oldhashed))
				return false;
			user.Password = ShaHasher.ComputeHash(Encoding.UTF8.GetBytes(info.Password));
			await DbContext.SaveChangesAsync();
			return true;
		}
		public async Task<User> DeleteUser(uint id, LogInInfo info)
		{
			var user = await DbContext.Users.FindAsync(id);
			if (user.Username == info.Username &&
			user.Password.SequenceEqual(ComputeHash(info.Password))) {
				DbContext.Users.Remove(user);
				await DbContext.SaveChangesAsync();
				return User.FromDBE(user);
			}
			return null;
		}
		public async Task<IEnumerable<uint>> GetPins(uint userid)
		{
			var user = await DbContext.Users.AsNoTracking().Include(u => u.UserSettings).ThenInclude(us => us.Pins).FirstOrDefaultAsync(u => u.Id == userid);
			return user.UserSettings.Pins.Select(i => i.InventoryId);
		}
		public async Task<EPinResult> PinInventory(uint userid, uint id)
		{
			var user = await DbContext.Users.Include(u => u.UserSettings).ThenInclude(us => us.Pins).FirstOrDefaultAsync(u => u.Id == userid);
			var inv = await DbContext.Inventories.FindAsync(id);
			if ((inv.OwnerId == userid || inv.WorldVisible)) {
				DBEUserInventoryPin pin;
				if ((pin = user.UserSettings.Pins.SingleOrDefault(i => i.InventoryId == inv.Id)) != null)
					return EPinResult.AlreadyPinned;
				user.UserSettings.Pins.Add(new DBEUserInventoryPin{
					Inventory = inv
				});
				await DbContext.SaveChangesAsync();
				return EPinResult.Ok;
			}
			return EPinResult.InventoryNotFound;
		}
		public async Task<EPinResult> UnpinInventory(uint userid, uint id)
		{
			var user = await DbContext.Users.Include(u => u.UserSettings).ThenInclude(us => us.Pins).FirstOrDefaultAsync(u => u.Id == userid);
			var inv = await DbContext.Inventories.FindAsync(id);
			if (inv.OwnerId == userid || inv.WorldVisible) {
				DBEUserInventoryPin pin;
				if ((pin = user.UserSettings.Pins.SingleOrDefault(i => i.InventoryId == inv.Id)) == null)
					return EPinResult.NotPinned;
				user.UserSettings.Pins.Remove(pin);
				await DbContext.SaveChangesAsync();
				return EPinResult.Ok;
			}
			return EPinResult.InventoryNotFound;
		}
	}
}
