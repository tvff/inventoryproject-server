using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InventoryProject.Models;
using InventoryProject.Services;
namespace InventoryProject.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class InventoryController : ControllerBase
	{
		private readonly InventoryService IS;
		private readonly SessionService SS;
		private Models.DBE.DBEUserSession Session;
		public InventoryController(InventoryService iserv, SessionService sserv)
		{
			IS = iserv;
			SS = sserv;
		}
		///<summary>Get all inventories that you're allowed to see, without content.</summary>
		[HttpGet]
		public async Task<ActionResult<IEnumerable<InventoryBase>>> GetInventories()
		{
			Session = await SS.GetSession(Request);
			if (Session != null)
				return Ok(await IS.GetInventories(Session.User.Id));
			return Ok(await IS.GetInventories());
		}
		///<summary>Get an inventory and its contents.</summary>
		[HttpGet("{id}")]
		public async Task<ActionResult<Inventory>> GetInventory(uint id)
		{
			Session = await SS.GetSession(Request);
			var inv = await IS.GetInventoryWithContent(id);
			if (inv == null)
				return NotFound(new ApiError(ApiErrorCodes.InventoryNotFound));
			if (inv.WorldVisible || (Session != null && Session.User.Id == inv.OwnerId))
				return inv;
			else
				return BadRequest(new ApiError(ApiErrorCodes.NotAllowed));
		}
		
		///<summary>Create a new inventory.</summary>
		[HttpPost]
		public async Task<ActionResult<Inventory>> PostInventory(NewInventory inv)
		{
			Session = await SS.GetSession(Request);
			if (Session == null)
				return Unauthorized(new ApiError(ApiErrorCodes.NeedAuth));
			if (!ModelState.IsValid)
				return BadRequest(new ApiError(ApiErrorCodes.BadInputData));
			var newInventory = await IS.CreateInventory(inv, Session.User.Id);
			return CreatedAtAction(nameof(GetInventory),new {id = newInventory.Id},newInventory);
		}
		///<summary>Edit an inventory. Note that this uses PATCH but works like PUT!</summary>
		[HttpPatch("{id}")]
		public async Task<ActionResult<Inventory>> EditInventory(uint id,[FromBody]NewInventory inv)
		{
			Session = await SS.GetSession(Request);
			if (Session == null)
				return Unauthorized(new ApiError(ApiErrorCodes.NeedAuth));
			if (!ModelState.IsValid)
				return BadRequest(new ApiError(ApiErrorCodes.BadInputData));
			var inventory = await IS.GetInventory(id);
			if (inventory == null)
				return NotFound(new ApiError(ApiErrorCodes.InventoryNotFound));
			if (inventory.OwnerId != Session.User.Id)
				return BadRequest(new ApiError(ApiErrorCodes.NotAllowed));
			return await IS.EditInventory(id,inv);
		}
		///<summary>Delete an inventory.</summary>
		[HttpDelete("{id}")]
		public async Task<ActionResult<Inventory>> DeleteInventory(uint id)
		{
			Session = await SS.GetSession(Request);
			if (Session == null)
				return Unauthorized(new ApiError(ApiErrorCodes.NeedAuth));
			var inventory = await IS.GetInventory(id);
			if (inventory == null)
				return NotFound(new ApiError(ApiErrorCodes.InventoryNotFound));
			if (inventory.OwnerId != Session.User.Id)
				return BadRequest(new ApiError(ApiErrorCodes.NotAllowed));
			return await IS.DeleteInventory(inventory.Id);
		}
		///<summary>Add an item to an inventory.</summary>
		// This is probably not the way to go.
		[HttpPut("{id}")]
		public async Task<ActionResult<OwnedItem>> PutItemInInventory(uint id, EntityId item)
		{
			Session = await SS.GetSession(Request);
			if (Session == null)
				return Unauthorized(new ApiError(ApiErrorCodes.NeedAuth));
			try
			{
				var inventory = await IS.GetInventory(id);
				if (inventory == null)
					return NotFound(new ApiError(ApiErrorCodes.InventoryNotFound));
				if (inventory.OwnerId != Session.User.Id)
					return BadRequest(new ApiError(ApiErrorCodes.NotAllowed));
				return await IS.AddItemToInventory(id,item.Id);
			}
			catch (NullReferenceException)
			{
				return BadRequest(new ApiError(ApiErrorCodes.ItemOrInventoryNotFound));
			}
		}
		///<summary>Remove an item from an inventory.</summary>
		[HttpDelete("{id}/{itemId}")]
		public async Task<ActionResult<OwnedItem>> RemoveItemFromInventory(uint id, uint itemId)
		{
			Session = await SS.GetSession(Request);
			if (Session == null)
				return Unauthorized(new ApiError(ApiErrorCodes.NeedAuth));
			try {
				var inventory = await IS.GetInventory(id);
				if (inventory == null)
					return NotFound(new ApiError(ApiErrorCodes.InventoryNotFound));
				if (inventory.OwnerId != Session.User.Id)
					return BadRequest(new ApiError(ApiErrorCodes.NotAllowed));
				return await IS.RemoveItemFromInventory(id,itemId);
			}
			catch (Exception) {
				return BadRequest(new ApiError(ApiErrorCodes.ItemOrInventoryNotFound));
			}
		}
	}
}
