using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using InventoryProject.Models;
using InventoryProject.Services;
namespace InventoryProject.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class TagController : ControllerBase
	{
		private readonly InventoryService IS;
		public TagController(InventoryService service)
		{
			IS = service;
		}
		///<summary>Get all tags.</summary>
		[HttpGet]
		public async Task<ActionResult<IEnumerable<Tag>>> GetTags()
		{
			return Ok(await IS.GetTags());
		}
		///<summary>Get a specific tag.</summary>
		[HttpGet("{id}")]
		public async Task<ActionResult<Tag>> GetTag(uint id)
		{
			var tag = await IS.GetTag(id);
			if (tag == null)
				return NotFound(new ApiError(ApiErrorCodes.TagNotFound));
			return Ok(tag);
		}
		///<summary>Create a new tag.</summary>
		[HttpPost]
		public async Task<ActionResult<Tag>> PostTag([FromBody] NewTag tag)
		{
			if (!ModelState.IsValid)
				return BadRequest(new ApiError(ApiErrorCodes.BadInputData));
			var newTag = await IS.AddTag(tag);
			return CreatedAtAction(nameof(GetTag),new { id = newTag.Id },newTag);
		}
		///<summary>Delete a tag.</summary>
		[HttpDelete("{id}")]
		public async Task<ActionResult<Tag>> DeleteTag(uint id)
		{
			try {
				var tag = await IS.DeleteTag(id);
				return Ok(tag);
			}
			catch (KeyNotFoundException) {
				return NotFound(new ApiError(ApiErrorCodes.TagNotFound));
			}
		}
	}
}
