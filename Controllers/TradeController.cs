using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using InventoryProject.Models;
using InventoryProject.Services;
namespace InventoryProject.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class TradeController : ControllerBase
	{
		private readonly SessionService SS;
		private readonly TradeService TS;
		private Models.DBE.DBEUserSession Session;
		public TradeController(SessionService sess, TradeService tsess)
		{
			SS = sess;
			TS = tsess;
		}
		///<summary>Get all your current sent and received trade offers.</summary>
		[HttpGet]
		public async Task<ActionResult<IEnumerable<TradeOffer>>> GetAll()
		{
			Session = await SS.GetSession(Request);
			if (Session == null)
				return Unauthorized(new ApiError(ApiErrorCodes.NeedAuth));
			return Ok(await TS.GetAll(Session.User.Id));
		}
		///<summary>Get a specific trade offer.</summary>
		[HttpGet("{id}")]
		public async Task<ActionResult<TradeOffer>> Get(uint id)
		{
			Session = await SS.GetSession(Request);
			if (Session == null)
				return Unauthorized(new ApiError(ApiErrorCodes.NeedAuth));
			var res = await TS.Get(id,Session.User.Id);
			if (res != null)
				return Ok(res);
			return NotFound(new ApiError(ApiErrorCodes.TradeOfferNotFound));
		}
		///<summary>Post a new trade offer.</summary>
		[HttpPost]
		public async Task<ActionResult<TradeOffer>> Post([FromBody] NewTradeOffer offer)
		{
			if (!ModelState.IsValid)
				return BadRequest(new ApiError(ApiErrorCodes.BadInputData));
			Session = await SS.GetSession(Request);
			if (Session == null)
				return Unauthorized(new ApiError(ApiErrorCodes.NeedAuth));
			var res = await TS.New(offer,Session.User.Id);
			if (res != null)
				return CreatedAtAction(nameof(Get),new { id = res.Id },res);
			return BadRequest(new ApiError(ApiErrorCodes.BadInputData));
		}
		///<summary>Delete (cancel) one of your trade offers.</summary>
		[HttpDelete("{id}")]
		public async Task<ActionResult<TradeOffer>> Delete(uint id)
		{
			Session = await SS.GetSession(Request);
			if (Session == null)
				return Unauthorized(new ApiError(ApiErrorCodes.NeedAuth));
			var res = await TS.Delete(id, Session.User.Id);
			if (res != null)
				return Ok(res);
			return BadRequest(new ApiError(ApiErrorCodes.Generic));
		}
		///<summary>Respond to a received trade offer.</summary>
		[HttpPost("{id}")]
		public async Task<ActionResult<TradeOffer>> Respond(uint id,[FromBody] TradeOfferResponse response)
		{
			if (!ModelState.IsValid)
				return BadRequest(new ApiError(ApiErrorCodes.BadInputData));
			Session = await SS.GetSession(Request);
			if (Session == null)
				return Unauthorized(new ApiError(ApiErrorCodes.NeedAuth));
			switch (response.Response){
				case "accept":
					var res = TS.AcceptOffer(id,Session.User.Id,response.Destination);
					if (res != null)
						return Ok(res);
					break;
				case "decline":
					var resd = TS.RejectOffer(id,Session.User.Id);
					if ( resd != null)
						return Ok(resd);
					break;
				default:
					return BadRequest(new ApiError(ApiErrorCodes.BadInputData));
			}
			return BadRequest(new ApiError(ApiErrorCodes.BadInputData));
		}
	}
}
