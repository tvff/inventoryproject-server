using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using InventoryProject.Models;
using InventoryProject.Models.DBE;
using InventoryProject.Services;
namespace InventoryProject.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UserController : ControllerBase
	{
		private readonly UserService US;
		private readonly SessionService SS;
		private DBEUserSession Session;
		public UserController(UserService user, SessionService session)
		{
			US = user;
			SS = session;
		}
		///<summary>Get all users.</summary>
		[HttpGet]
		public async Task<ActionResult<IEnumerable<User>>> Get()
		{
			return Ok(await US.GetAll());
		}
		///<summary>Get a specific user by id.</summary>
		[HttpGet("{id}")]
		public async Task<ActionResult<User>> GetUser(uint id)
		{
			var user = await US.GetUser(id);
			if (user != null)
				return Ok(user);
			return NotFound(new ApiError(ApiErrorCodes.UserNotFound));
		}
		///<summary>Authenticate, or "log in".</summary>
		[HttpPost("authenticate")]
		public async Task<ActionResult<SessionAndUser>> Authenticate(LogInInfo info)
		{
			var name = Request.Headers["User-Agent"];
			if (!ModelState.IsValid)
				return BadRequest(new ApiError(ApiErrorCodes.BadLoginInfo));
			var session = await US.Authenticate(info);
			if (session == null)
				return BadRequest(new ApiError(ApiErrorCodes.BadLogin));
			return Ok(await SS.GenerateSessionForUser(session.Id,name));
		}
		///<summary>End a session, or all sessions. Also known as logging out.</summary>
		[HttpPost("logout")]
		public async Task<ActionResult<object>> Logout([FromBody] LogOutInfo info)
		{
			Session = await SS.GetSession(Request);
			if (Session == null)
				return BadRequest(new ApiError(ApiErrorCodes.CantLogOutAsGuest));
			if (info.AllSessions)
				await SS.EndAllSessions(Session.User.Id);
			else
				await SS.EndSession(Session.Id);
			return Ok(new {msg="Logged out nicely!"});
		}
		///<summary>Register and create a new user.</summary>
		[HttpPost("register")]
		public async Task<ActionResult<User>> Register([FromBody] NewUserInfo info)
		{
			if (!ModelState.IsValid)
				return BadRequest(new ApiError(ApiErrorCodes.BadInputData));
			// Username has to be unique..
			if (await US.GetUser(info.Username) != null)
				return BadRequest(new ApiError(ApiErrorCodes.UsernameTaken));
			var newUser = await US.CreateUser(info);
			return Ok(newUser);
		}
		///<summary>Change current user acccount's password.</summary>
		[HttpPost("changepassword")]
		public async Task<ActionResult<object>> ChangePassword([FromBody] ChangePasswordInfo info)
		{
			if (!ModelState.IsValid)
				return BadRequest(new ApiError(ApiErrorCodes.BadInputData));
			Session = await SS.GetSession(Request);
			if (Session == null)
				return Unauthorized(new ApiError(ApiErrorCodes.NeedAuth));
			var res = await US.ChangePassword(Session.User.Id,info);
			if (res)
				return Ok(new {msg="Password changed nicely!"});
			else
				return BadRequest(new ApiError(ApiErrorCodes.WrongPassword));
		}
		///<summary>Delete user account. Be very very careful.</summary>
		[HttpPost("delete")]
		public async Task<ActionResult<User>> Delete([FromBody] LogInInfo info)
		{
			Session = await SS.GetSession(Request);
			if (Session == null)
				return Unauthorized(new ApiError(ApiErrorCodes.NeedAuth));
			if (!ModelState.IsValid || Session.User.Username != info.Username)
				return BadRequest(new ApiError(ApiErrorCodes.BadLoginInfo));
			var nukeduser = await US.DeleteUser(Session.User.Id,info);
			if (nukeduser == null)
				return BadRequest(new ApiError(ApiErrorCodes.WrongPassword));
			return nukeduser;
		}
		///<summary>Gets all currently active sessions, and which one is currently used.
		/// Useful for seeing if your session token is still valid.</summary>
		[HttpGet("session")]
		public async Task<ActionResult<UserSessionData>> GetSessions()
		{
			Session = await SS.GetSession(Request);
			if (Session == null)
				return Unauthorized(new ApiError(ApiErrorCodes.NeedAuth));
			var sessions = await SS.GetSessions(Session.User.Id);
			var user = Models.User.FromDBE(Session.User);
			return Ok(new UserSessionData{Sessions = sessions, CurrentSession = Session.Id, User = user});
		}
		///<summary>Pin an inventory so you can easily access it.</summary>
		[HttpPost("pin/{id}")]
		public async Task<ActionResult<object>> AddInventoryPin(uint id)
		{
			Session = await SS.GetSession(Request);
			if (Session == null)
				return Unauthorized(new ApiError(ApiErrorCodes.NeedAuth));
			var res = EPinResult.InventoryNotFound;
			try{
				res = await US.PinInventory(Session.User.Id,id);
			}
			catch(Exception e){
				Console.WriteLine("Swallowing exception *burp* {0}",e.Message);
			}
			switch (res){
				case EPinResult.Ok: return Ok(new {msg="Pinned that nicely!"});
				case EPinResult.AlreadyPinned: return BadRequest(new ApiError(ApiErrorCodes.AlreadyPinned));
				case EPinResult.InventoryNotFound: return NotFound(new ApiError(ApiErrorCodes.InventoryNotFound));
				default: return BadRequest(new ApiError(ApiErrorCodes.Generic));
			}
		}
		///<summary>Unpin an inventory so you can't access it so easily anymore.</summary>
		[HttpDelete("pin/{id}")]
		public async Task<ActionResult<object>> RemoveInventoryPin(uint id)
		{
			Session = await SS.GetSession(Request);
			if (Session == null)
				return Unauthorized(new ApiError(ApiErrorCodes.NeedAuth));
			var res = EPinResult.InventoryNotFound;
			try{
				res = await US.UnpinInventory(Session.User.Id,id);
			}
			catch(Exception e){
				Console.WriteLine("Swallowing exception *burp* {0}",e.Message);
			}
			switch (res){
				case EPinResult.Ok: return Ok(new {msg="Unpinned that nicely!"});
				case EPinResult.NotPinned: return BadRequest(new ApiError(ApiErrorCodes.NotPinned));
				case EPinResult.InventoryNotFound: return NotFound(new ApiError(ApiErrorCodes.InventoryNotFound));
				default: return BadRequest(new ApiError(ApiErrorCodes.Generic));
			}
		}
		///<summary>Get your pinned inventories.</summary>
		[HttpGet("pin")]
		public async Task<ActionResult<IEnumerable<uint>>> GetPins()
		{
			Session = await SS.GetSession(Request);
			if (Session == null)
				return Unauthorized(new ApiError(ApiErrorCodes.NeedAuth));
			return Ok(await US.GetPins(Session.User.Id));
		}
	}
}
