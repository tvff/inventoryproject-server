using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using InventoryProject.Models;
using InventoryProject.Services;
namespace InventoryProject.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ItemController : ControllerBase
	{
		private readonly InventoryService IS;
		private readonly SessionService SS;
		private Models.DBE.DBEUserSession Session;

		public ItemController(InventoryService iservice, SessionService sservice)
		{
			IS = iservice;
			SS = sservice;
		}

		///<summary>List all known items</summary>
		[HttpGet()]
		public async Task<ActionResult<IEnumerable<UniqueItem>>> GetItems()
		{
			return Ok(await IS.GetAllItems());
		}
		///<summary>Get a specific item.</summary>
		[HttpGet("{id}")]
		public async Task<ActionResult<UniqueItem>> GetItem(uint id)
		{
			var item = await IS.GetItem(id);
			if (item == null)
				return NotFound(new ApiError(ApiErrorCodes.ItemNotFound));
			return item;
		}
		///<summary>Get a specific owned item.</summary>
		[HttpGet("owned/{id}")]
		public async Task<ActionResult<OwnedItem>> GetOwnedItem(uint id)
		{
			Session = await SS.GetSession(Request);
			if (Session == null)
				return Unauthorized(new ApiError(ApiErrorCodes.NeedAuth));
			var item = await IS.GetOwnedItem(id,Session.User.Id);
			if (item == null)
				return NotFound(new ApiError(ApiErrorCodes.ItemNotFound));
			return item;
		}
		///<summary>Create a new item.</summary>
		[HttpPost]
		public async Task<ActionResult<UniqueItem>> PostItem(NewUniqueItem item)
		{
			Session = await SS.GetSession(Request);
			if (Session == null)
				return Unauthorized(new ApiError(ApiErrorCodes.NeedAuth));
			try {
				var newItem = await IS.AddItem(item,Session.User.Id);
				return CreatedAtAction(nameof(GetItem),new {id = newItem.Id},newItem);
			}
			catch (Exception) {
				return BadRequest(new ApiError(ApiErrorCodes.BadInputData));
			}
		}
		///<summary>Modify an item.</summary>
		[HttpPut("{id}")]
		public async Task<ActionResult<UniqueItem>> PutItem(uint id, NewUniqueItem item)
		{
			Session = await SS.GetSession(Request);
			if (Session == null)
				return Unauthorized(new ApiError(ApiErrorCodes.NeedAuth));
			var olditem = await IS.GetItem(id);
			if (olditem.OwnerId != Session.User.Id)
				return BadRequest(new ApiError(ApiErrorCodes.NotAllowed));
			return await IS.EditItem(id,item);
		}
		///<summary>Add tags to an item.</summary>
		[HttpPost("{id}/tags")]
		public async Task<ActionResult<UniqueItem>> AddTags(uint id, [FromBody] uint[] tags)
		{
			var item = await IS.AddTagsToItem(id,tags);
			if (item == null)
				return NotFound(new ApiError(ApiErrorCodes.ItemNotFound));
			return item;
		}
		///<summary>Remove tags from an item.</summary>
		[HttpDelete("{id}/tags")]
		public async Task<ActionResult<UniqueItem>> RemoveTags(uint id, [FromBody] uint[] tags)
		{
			var item = await IS.RemoveTagsFromItem(id,tags);
			return item;
		}
		///<summary>Delete an item.</summary>
		[HttpDelete("{id}")]
		public async Task<ActionResult<UniqueItem>> DeleteItem(uint id)
		{
			Session = await SS.GetSession(Request);
			if (Session == null)
				return Unauthorized(new ApiError(ApiErrorCodes.NeedAuth));
			var item = await IS.GetItem(id);
			if (item.OwnerId != Session.User.Id)
				return BadRequest(new ApiError(ApiErrorCodes.NotAllowed));
			return await IS.DeleteItem(id);
		}
	}
}
