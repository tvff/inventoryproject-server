# InventoryProject API server
This is the API server for InventoryProject.
## Building and Running
To start the server, simply run `dotnet run`. This will by default use the in memory database backend.
The server will listen on ports 5000 (HTTP) and 5001 (HTTPS).
You can also use the `mysql` or `sqlite` backend if you want something with a bit more persistence.
To do that make a text file `config.json` in this directory. Sample config:
### config.json
```json
{
	"efbackend": "mysql",
	"dbconnectionstring": "put-your-db-connection-string-here"
}
```
And then run `dotnet ef migrations add Initial` to create the initial migration. If that is successful `dotnet run` should Just Work™.
## Root?
Root is the default user. You can log in as Root with the username `root` and password `secret`.
