using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using InventoryProject.Models;
using System.IO;
using System.Reflection;

namespace InventoryProject
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.Configure<ApiBehaviorOptions>(options => {
				options.SuppressModelStateInvalidFilter = true;
			});
			services.AddDbContext<InvDbContext>(opt => InvDbContext.Configure(opt));
			services.AddTransient<Services.InventoryService>();
			services.AddTransient<Services.UserService>();
			services.AddTransient<Services.SessionService>();
			services.AddTransient<Services.TradeService>();
			services.AddSwaggerGen(c => {
					c.SwaggerDoc("v1", new OpenApiInfo { Title = "Inventory API", Version="v0"});
					var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
					var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
					c.IncludeXmlComments(xmlPath);
					});
			services.AddControllers();
		}
		private void ConfigureDb(IServiceProvider appServices)
		{
			using (var scope = appServices.GetRequiredService<IServiceScopeFactory>().CreateScope()) {
				using (var dbc = scope.ServiceProvider.GetService<InvDbContext>()) {
					// Actual databases need to be inited properly and all that.
					if (AppConfiguration.Instance.DatabaseBackend != EFDatabaseBackend.InMemory)
						dbc.Database.Migrate();
					scope.ServiceProvider.GetService<Services.UserService>().Init();
				}
			}
		}
		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			ConfigureDb(app.ApplicationServices);
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			app.UseRouting();
			app.UseAuthorization();
			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
			app.UseSwagger();
			app.UseSwaggerUI(c => {
					c.SwaggerEndpoint("v1/swagger.json", "Inventory API v0");
					});
		}
	}
}
